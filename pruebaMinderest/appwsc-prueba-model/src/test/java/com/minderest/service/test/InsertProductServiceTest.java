package com.minderest.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.minderest.model.InsertProductRequest;
import com.minderest.model.ProductoCliente;
import com.minderest.repository.ProductRepository;
import com.minderest.service.InsertProductService;
import com.minderest.service.InsertProductServiceImpl;

@ExtendWith(MockitoExtension.class)
class InsertProductServiceTest {


	@InjectMocks
	private InsertProductService insertProductService = new InsertProductServiceImpl();

	@Mock
	private ProductRepository productRepository;

	@Test
	void getInsertProductServiceTest() throws Exception {

		List<ProductoCliente> listaRespuesta = new ArrayList<>();

		Mockito.when(productRepository.getProductFromClient(Mockito.any(Integer.class), Mockito.any(Integer.class),
				Mockito.any(String.class))).thenReturn(listaRespuesta);

		InsertProductRequest insertProductRequest = generarMockInsertProductRequest();

		insertProductService.insertProduct(insertProductRequest);
		
	}

	private InsertProductRequest generarMockInsertProductRequest() {

		InsertProductRequest result = new InsertProductRequest();

		result.setClientId(101);
		result.setProductId(1);
		result.setProductName("Tele 4K");

		return result;
	}

}
