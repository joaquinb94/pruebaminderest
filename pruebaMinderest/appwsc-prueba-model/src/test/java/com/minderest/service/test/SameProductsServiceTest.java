package com.minderest.service.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.minderest.model.ProductoEquivalenteCliente;
import com.minderest.model.SameProduct;
import com.minderest.model.SameProductsRequest;
import com.minderest.model.SameProductsResponse;
import com.minderest.repository.ProductRepository;
import com.minderest.service.SameProductsService;
import com.minderest.service.SameProductsServiceImpl;

@ExtendWith(MockitoExtension.class)
class SameProductsServiceTest {


	@InjectMocks
	private SameProductsService sameProductsService = new SameProductsServiceImpl();

	@Mock
	private ProductRepository productRepository;

	@Test
	void getSameProductsServiceTest() throws Exception {

		SameProductsResponse response = generarMockSameProductsResponse();

		Mockito.when(productRepository.getSameProducts(Mockito.any(Integer.class), Mockito.any(Integer.class)))
				.thenReturn(response);
		
		List<ProductoEquivalenteCliente> listResponse = new ArrayList<>();
		ProductoEquivalenteCliente productoEquivalenteCliente = new ProductoEquivalenteCliente();
		productoEquivalenteCliente.setIdCliente(102);
		productoEquivalenteCliente.setIdProductoEquivalente(2);
		productoEquivalenteCliente.setIdProducto(3);
		listResponse.add(productoEquivalenteCliente);

		Mockito.when(productRepository.getEquivalent(Mockito.any(List.class)))
				.thenReturn(listResponse);

		SameProductsRequest sameProductsRequest = generarMockSameProductsRequest();

		SameProductsResponse sameProductsResponse = sameProductsService.sameProducts(sameProductsRequest);
		assertNotNull(sameProductsResponse.getSameProducts());
	}

	private SameProductsResponse generarMockSameProductsResponse() {
		SameProductsResponse response = new SameProductsResponse();

		List<SameProduct> list = new ArrayList<SameProduct>();
		SameProduct sp = new SameProduct();
		sp.setClientName("Media Markt");
		sp.setProductName("Tostadora Inteligente");

		SameProduct sp2 = new SameProduct();
		sp2.setClientName("Worten");
		sp2.setProductName("Smart Tuestapan");

		list.add(sp);
		list.add(sp2);

		response.setSameProducts(list);
		return response;

	}

	private SameProductsRequest generarMockSameProductsRequest() {

		SameProductsRequest result = new SameProductsRequest();

		result.setClientId(102);
		result.setProductId(3);

		return result;
	}

}
