package com.minderest.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.minderest.model.EquivalentProductRequest;
import com.minderest.model.ProductoEquivalenteCliente;
import com.minderest.repository.ProductRepository;
import com.minderest.service.EquivalentProductService;
import com.minderest.service.EquivalentProductServiceImpl;

@ExtendWith(MockitoExtension.class)
class EquivalentProductServiceTest {

	@InjectMocks
	private EquivalentProductService equivalentProductService = new EquivalentProductServiceImpl();

	@Mock
	private ProductRepository productRepository;

	@Test
	void getEquivalentProductServiceTest() throws Exception {

		List<ProductoEquivalenteCliente> listaRespuesta = mockGetProducts();

		Mockito.when(productRepository.getProducts(Mockito.any(List.class))).thenReturn(listaRespuesta);

		EquivalentProductRequest equivalentProductRequest = generarMockEquivalentProductRequest();

		equivalentProductService.equivalentProduct(equivalentProductRequest);

	}

	private List<ProductoEquivalenteCliente> mockGetProducts() {
		List<ProductoEquivalenteCliente> response = new ArrayList<ProductoEquivalenteCliente>();
		ProductoEquivalenteCliente p = new ProductoEquivalenteCliente();
		p.setIdProducto(1);
		p.setIdProductoEquivalente(1);
		p.setIdCliente(101);

		ProductoEquivalenteCliente p2 = new ProductoEquivalenteCliente();
		p2.setIdProducto(3);
		p2.setIdProductoEquivalente(3);
		p2.setIdCliente(102);

		response.add(p);
		response.add(p2);
		return response;
	}

	private EquivalentProductRequest generarMockEquivalentProductRequest() {

		EquivalentProductRequest result = new EquivalentProductRequest();

		result.setClientId(101);
		result.setProductId1(1);
		result.setProductId2(4);

		return result;
	}
}
