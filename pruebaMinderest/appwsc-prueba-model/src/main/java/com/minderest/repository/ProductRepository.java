package com.minderest.repository;

import java.util.List;

import com.minderest.model.ProductoCliente;
import com.minderest.model.ProductoEquivalenteCliente;
import com.minderest.model.SameProductsResponse;

public interface ProductRepository {

	public List<ProductoCliente> getProductFromClient(Integer productId, Integer clientId, String productName) throws Exception;
	
	public void insertProduct(Integer productId, Integer clientId, String productName) throws Exception;

	public List<ProductoEquivalenteCliente> getProducts(List<Integer> listaProd) throws Exception;
	
	public void updateEquivalents(Integer idMismoProd1, Integer productId2) throws Exception;
	
	public SameProductsResponse getSameProducts(Integer clientId, Integer equivalentId) throws Exception;

	public List<ProductoEquivalenteCliente> getEquivalent(List<Integer> listaProd) throws Exception;

}
