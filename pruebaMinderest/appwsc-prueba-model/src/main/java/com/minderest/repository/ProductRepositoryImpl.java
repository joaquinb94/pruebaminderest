package com.minderest.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.minderest.model.ProductoCliente;
import com.minderest.model.ProductoEquivalenteCliente;
import com.minderest.model.SameProduct;
import com.minderest.model.SameProductsResponse;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Value("${getProductFromClient}")
	private String getProductFromClient;

	@Value("${insertTablaProducto}")
	private String insertTablaProducto;

	@Value("${insertTablaProductoCliente}")
	private String insertTablaProductoCliente;

	@Value("${getEquivalent}")
	private String getEquivalent;

	@Value("${getSameProducts}")
	private String getSameProducts;

	@Value("${updateEquivalents}")
	private String updateEquivalents;

	@Override
	public List<ProductoCliente> getProductFromClient(Integer productId, Integer clientId, String productName)
			throws Exception {

		List<ProductoCliente> result = null;

		try {
			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("productId", productId);
			params.addValue("clientId", clientId);
			params.addValue("productName", productName);

			result = this.namedParameterJdbcTemplate.query(this.getProductFromClient, params,
					this.productoClienteMapper);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

		return result;
	}

	private final RowMapper<ProductoCliente> productoClienteMapper = new RowMapper<ProductoCliente>() {
		@Override
		public ProductoCliente mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final ProductoCliente prodCliente = new ProductoCliente();
			prodCliente.setIdProducto(rs.getInt("ID_PRODUCTO"));
			prodCliente.setIdCliente(rs.getInt("ID_CLIENTE"));
			prodCliente.setNombreProdCliente(rs.getString("NOMBRE_PROD_CLIENTE"));
			return prodCliente;
		}
	};

	@Override
	public void insertProduct(Integer productId, Integer clientId, String productName) throws Exception {

		try {
			this.jdbcTemplate.update(this.insertTablaProducto, productId, productId);
			this.jdbcTemplate.update(this.insertTablaProductoCliente, productId, clientId, productName);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

	}

	@Override
	public List<ProductoEquivalenteCliente> getProducts(List<Integer> listaProd) throws Exception {

		List<ProductoEquivalenteCliente> result = null;

		try {
			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("listaProd", listaProd);

			result = this.namedParameterJdbcTemplate.query(this.getEquivalent, params,
					this.productoEquivalenteClienteMapper);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

		return result;
	}

	@Override
	public void updateEquivalents(Integer idMismoProd1, Integer productId2) throws Exception {

		try {
			this.jdbcTemplate.update(this.updateEquivalents, idMismoProd1, productId2);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

	}

	@Override
	public List<ProductoEquivalenteCliente> getEquivalent(List<Integer> listaProd) throws Exception {

		List<ProductoEquivalenteCliente> result = null;
		try {

			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("listaProd", listaProd);

			result = this.namedParameterJdbcTemplate.query(this.getEquivalent, params,
					this.productoEquivalenteClienteMapper);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

		return result;

	}

	private final RowMapper<ProductoEquivalenteCliente> productoEquivalenteClienteMapper = new RowMapper<ProductoEquivalenteCliente>() {
		@Override
		public ProductoEquivalenteCliente mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final ProductoEquivalenteCliente productoEquivalenteCliente = new ProductoEquivalenteCliente();
			productoEquivalenteCliente.setIdCliente(rs.getInt("ID_CLIENTE"));
			productoEquivalenteCliente.setIdProductoEquivalente(rs.getInt("ID_MISMO_PRODUCTO"));
			productoEquivalenteCliente.setIdProducto(rs.getInt("ID_PRODUCTO"));
			return productoEquivalenteCliente;
		}
	};

	@Override
	public SameProductsResponse getSameProducts(Integer clientId, Integer equivalentId) throws Exception {

		SameProductsResponse result = new SameProductsResponse();
		List<SameProduct> sameProductList = new ArrayList<>();

		try {
			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("clientId", clientId);
			params.addValue("equivalentId", equivalentId);

			sameProductList = this.namedParameterJdbcTemplate.query(this.getSameProducts, params,
					this.sameProductMapper);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

		result.setSameProducts(sameProductList);
		return result;
	}

	private final RowMapper<SameProduct> sameProductMapper = new RowMapper<SameProduct>() {
		@Override
		public SameProduct mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final SameProduct sameProduct = new SameProduct();
			sameProduct.setClientName(rs.getString("NOMBRE_CLIENTE"));
			sameProduct.setProductName(rs.getString("NOMBRE_PROD_CLIENTE"));
			return sameProduct;
		}
	};

}
