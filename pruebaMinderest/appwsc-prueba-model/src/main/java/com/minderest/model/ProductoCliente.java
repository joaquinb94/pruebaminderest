package com.minderest.model;

public class ProductoCliente {
	
	private Integer idProducto;
	private Integer idCliente;
	private String nombreProdCliente;
	
	public ProductoCliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer productId) {
		this.idProducto = productId;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer clientId) {
		this.idCliente = clientId;
	}

	public String getNombreProdCliente() {
		return nombreProdCliente;
	}

	public void setNombreProdCliente(String productName) {
		this.nombreProdCliente = productName;
	}
	
	

}
