package com.minderest.model;

public class ProductoEquivalenteCliente {

	private Integer idProductoEquivalente;
	private Integer idProducto;
	private Integer idCliente;
	public ProductoEquivalenteCliente() {
		super();
	}
	public Integer getIdProductoEquivalente() {
		return idProductoEquivalente;
	}
	public void setIdProductoEquivalente(Integer idProductoEquivalente) {
		this.idProductoEquivalente = idProductoEquivalente;
	}
	public Integer getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	
	
	
}
