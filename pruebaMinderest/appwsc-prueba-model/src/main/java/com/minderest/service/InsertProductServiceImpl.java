package com.minderest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minderest.exception.ProductException;
import com.minderest.model.InsertProductRequest;
import com.minderest.model.ProductoCliente;
import com.minderest.repository.ProductRepository;

@Service
public class InsertProductServiceImpl implements InsertProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public void insertProduct(InsertProductRequest insertProductRequest) throws Exception {

		try {
			List<ProductoCliente> productos = productRepository.getProductFromClient(
					insertProductRequest.getProductId(), insertProductRequest.getClientId(),
					insertProductRequest.getProductName());

			if (productos.isEmpty()) {
				// El producto no existe y el cliente no tiene otro con ese nombre
				productRepository.insertProduct(insertProductRequest.getProductId(), insertProductRequest.getClientId(),
						insertProductRequest.getProductName());
			} else {
				for (ProductoCliente cp : productos) {
					if (cp.getIdProducto().intValue() == insertProductRequest.getProductId().intValue()) {
						throw new ProductException("[insertProduct] ERROR: El producto ya existe para el cliente");
					}
					if (cp.getNombreProdCliente().equals(insertProductRequest.getProductName())) {
						throw new ProductException("[insertProduct] ERROR: El cliente ya tiene un producto con ese nombre");
					}
				}
			}

		} catch (final Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

	}

}
