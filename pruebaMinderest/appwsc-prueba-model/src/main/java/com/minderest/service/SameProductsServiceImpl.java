package com.minderest.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minderest.exception.ProductException;
import com.minderest.model.ProductoEquivalenteCliente;
import com.minderest.model.SameProductsRequest;
import com.minderest.model.SameProductsResponse;
import com.minderest.repository.ProductRepository;

@Service
public class SameProductsServiceImpl implements SameProductsService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public SameProductsResponse sameProducts(SameProductsRequest sameProductsRequest) throws Exception {

		
		SameProductsResponse response = null;
		try {
			List<Integer> listaProd = new ArrayList<Integer>();
			listaProd.add(sameProductsRequest.getProductId());
			
			List<ProductoEquivalenteCliente> listResponse = productRepository.getEquivalent(listaProd);
			
			ProductoEquivalenteCliente productoEquivalenteCliente = null;
			
			if(listResponse != null && !listResponse.isEmpty()) {
				productoEquivalenteCliente = listResponse.get(0);
			}

			if (productoEquivalenteCliente!=null && productoEquivalenteCliente.getIdProductoEquivalente()!=null) {
				
				if(productoEquivalenteCliente.getIdCliente().intValue() != sameProductsRequest.getClientId().intValue()) {
					throw new ProductException("[sameProducts] ERROR: El producto que se consulta debe pertenecer al cliente");
					
				}
				else {
					// Obtenemos los productos que son equivalentes al de la entrada
					response = productRepository.getSameProducts(sameProductsRequest.getClientId(),productoEquivalenteCliente.getIdProductoEquivalente());
					
				}
			} else {
				throw new ProductException("[sameProducts] ERROR: El producto consultado no existe");
			}

		} catch (final Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}
		
		return response;

	}

}
