package com.minderest.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minderest.exception.ProductException;
import com.minderest.model.EquivalentProductRequest;
import com.minderest.model.ProductoEquivalenteCliente;
import com.minderest.repository.ProductRepository;

@Service
public class EquivalentProductServiceImpl implements EquivalentProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public void equivalentProduct(EquivalentProductRequest equivalentProductRequest) throws Exception {

		try {

			List<Integer> listaProd = new ArrayList<>();
			listaProd.add(equivalentProductRequest.getProductId1());
			listaProd.add(equivalentProductRequest.getProductId2());
			List<ProductoEquivalenteCliente> productos = productRepository.getProducts(listaProd);

			if (productos.size() == listaProd.size()) {
				// Si las listas tienen la misma longitud es que ambos productos existen
				Integer idMismoProd1;
				for (ProductoEquivalenteCliente p : productos) {
					if (p.getIdProducto().intValue() == equivalentProductRequest.getProductId1().intValue()) {
						if (p.getIdCliente().intValue() == equivalentProductRequest.getClientId().intValue()) {
							idMismoProd1 = p.getIdProductoEquivalente();
							productRepository.updateEquivalents(idMismoProd1, equivalentProductRequest.getProductId2());
						} else {
							throw new ProductException(
									"[equivalentProduct] ERROR: El producto 1 debe pertenecer al cliente");
						}

					}
				}

			} else {
				throw new ProductException(
						"[equivalentProduct] ERROR: Los productos deben existir para establecer una relación de equivalencia");
			}

		} catch (final Exception e) {
			System.out.println(e.getMessage());
			throw e;
		}

	}

}
