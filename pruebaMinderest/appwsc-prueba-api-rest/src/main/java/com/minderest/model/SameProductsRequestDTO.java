/*
 * com.minderest:appwsc-prueba-boot
 * API to integrate apps with repository services.
 *
 * OpenAPI spec version: 1.0.0-build1
 * Contact: joaquinbermudez94@gmail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.minderest.model;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * SameProductsRequest
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-12-24T10:22:41.422Z[GMT]")
public class SameProductsRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("productId")
	private Integer productId = null;

	@JsonProperty("clientId")
	private Integer clientId = null;

	public SameProductsRequestDTO productId(Integer productId) {
		this.productId = productId;
		return this;
	}

	/**
	 * Get productId
	 * 
	 * @return productId
	 **/
	@ApiModelProperty(value = "")
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public SameProductsRequestDTO clientId(Integer clientId) {
		this.clientId = clientId;
		return this;
	}

	/**
	 * Get clientId
	 * 
	 * @return clientId
	 **/
	@ApiModelProperty(value = "")
	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SameProductsRequestDTO sameProductsRequest = (SameProductsRequestDTO) o;
		return Objects.equals(this.productId, sameProductsRequest.productId)
				&& Objects.equals(this.clientId, sameProductsRequest.clientId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(productId, clientId);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SameProductsRequest {\n");

		sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
		sb.append("    clientId: ").append(toIndentedString(clientId)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
