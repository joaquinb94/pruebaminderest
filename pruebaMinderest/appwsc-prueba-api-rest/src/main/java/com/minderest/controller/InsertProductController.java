package com.minderest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.minderest.mapper.InsertProductRequestMapper;
import com.minderest.model.InsertProductRequest;
import com.minderest.model.InsertProductRequestDTO;
import com.minderest.service.InsertProductApi;
import com.minderest.service.InsertProductService;

@RestController
public class InsertProductController implements InsertProductApi {
	@Autowired
	private InsertProductRequestMapper insertProductRequestMapper;

	@Autowired
	private InsertProductService insertProductService;

	@Override
	public ResponseEntity<Void> insertProduct10(InsertProductRequestDTO insertProductRequestDTO) {

		System.out.println("[insertProduct] productId:" + insertProductRequestDTO.getProductId() + " productName:"
				+ insertProductRequestDTO.getProductName() + " clientId:" + insertProductRequestDTO.getClientId());

		ResponseEntity<Void> response = null;

		InsertProductRequest entrada = this.insertProductRequestMapper.asInsertProductRequest(insertProductRequestDTO);

		try {
			this.insertProductService.insertProduct(entrada);
			response = new ResponseEntity<>(HttpStatus.OK);
			System.out.println("OK");

		} catch (final Exception e) {
			response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
}