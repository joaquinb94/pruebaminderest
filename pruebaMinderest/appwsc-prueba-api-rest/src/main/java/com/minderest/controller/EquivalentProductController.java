package com.minderest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.minderest.mapper.EquivalentProductRequestMapper;
import com.minderest.model.EquivalentProductRequest;
import com.minderest.model.EquivalentProductRequestDTO;
import com.minderest.service.EquivalentProductApi;
import com.minderest.service.EquivalentProductService;

@RestController
public class EquivalentProductController implements EquivalentProductApi {
	@Autowired
	private EquivalentProductRequestMapper equivalentProductRequestMapper;

	@Autowired
	private EquivalentProductService equivalentProductService;

	@Override
	public ResponseEntity<Void> equivalentProduct10(EquivalentProductRequestDTO equivalentProductRequestDTO) {

		System.out.println("[equivalentProduct] productId1:" + equivalentProductRequestDTO.getProductId1()
				+ " productId2:" + equivalentProductRequestDTO.getProductId2() + " clientId:"
				+ equivalentProductRequestDTO.getClientId());

		ResponseEntity<Void> response = null;

		EquivalentProductRequest entrada = this.equivalentProductRequestMapper
				.asEquivalentProductRequest(equivalentProductRequestDTO);

		try {
			this.equivalentProductService.equivalentProduct(entrada);
			response = new ResponseEntity<>(HttpStatus.OK);
			System.out.println("OK");

		} catch (final Exception e) {
			response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
}