package com.minderest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.minderest.mapper.SameProductsRequestMapper;
import com.minderest.mapper.SameProductsResponseMapper;
import com.minderest.model.SameProductsRequest;
import com.minderest.model.SameProductsRequestDTO;
import com.minderest.model.SameProductsResponse;
import com.minderest.model.SameProductsResponseDTO;
import com.minderest.service.SameProductsApi;
import com.minderest.service.SameProductsService;

@RestController
public class SameProductsController implements SameProductsApi {
	@Autowired
	private SameProductsRequestMapper sameProductsRequestMapper;

	@Autowired
	private SameProductsResponseMapper sameProductsResponseMapper;

	@Autowired
	private SameProductsService sameProductsService;

	@Override
	public ResponseEntity<SameProductsResponseDTO> sameProducts10(SameProductsRequestDTO sameProductsRequestDTO) {

		System.out.println("[sameProducts] productId:" + sameProductsRequestDTO.getProductId() + " clientId:"
				+ sameProductsRequestDTO.getClientId());

		ResponseEntity<SameProductsResponseDTO> response = null;
		SameProductsResponseDTO respuesta;

		SameProductsRequest entrada = this.sameProductsRequestMapper.asSameProductsRequest(sameProductsRequestDTO);

		try {
			SameProductsResponse salida = this.sameProductsService.sameProducts(entrada);
			respuesta = this.sameProductsResponseMapper.asSameProductsResponseDTO(salida);

			if (respuesta == null) {
				response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				response = new ResponseEntity<>(respuesta, HttpStatus.OK);
				System.out.println("OK");
			}

		} catch (final Exception e) {
			response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
}