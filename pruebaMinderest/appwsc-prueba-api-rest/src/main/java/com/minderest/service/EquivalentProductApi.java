package com.minderest.service;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import com.minderest.model.EquivalentProductRequestDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.validation.Valid;

@Validated
@Api(value = "EquivalentProduct", description = "the EquivalentProduct API")
public interface EquivalentProductApi {

	default Optional<NativeWebRequest> getRequest() {
		return Optional.empty();
	}

	@ApiOperation(value = "Servicio que establece una relación de equivalencia entre dos productos", nickname = "equivalentProduct10", notes = "", tags = {
			"equivalentProduct" })

	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK."),
			@ApiResponse(code = 400, message = "Error validating entry params. "),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "Not Found") })
	@RequestMapping(value = "/1.0/equivalentProduct", produces = { "application/json" }, consumes = {
			"application/json" }, method = RequestMethod.POST)
	default ResponseEntity<Void> equivalentProduct10(
			@ApiParam(value = "equivalentProductEntry", required = true) @Valid @RequestBody EquivalentProductRequestDTO equivalentProductRequestDTO) {
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

	}
}
