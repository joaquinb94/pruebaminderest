package com.minderest.service;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import com.minderest.model.InsertProductRequestDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.validation.Valid;

@Validated
@Api(value = "InsertProduct", description = "the InsertProduct API")
public interface InsertProductApi {

	default Optional<NativeWebRequest> getRequest() {
		return Optional.empty();
	}

	@ApiOperation(value = "Servicio que inserta un nuevo producto en el sistema", nickname = "insertProduct10", notes = "", tags = {
			"insertProduct" })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK."),
			@ApiResponse(code = 400, message = "Error validating entry params. "),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "Not Found") })
	@RequestMapping(value = "/1.0/insertProduct", produces = { "application/json" }, consumes = {
			"application/json" }, method = RequestMethod.POST)
	default ResponseEntity<Void> insertProduct10(
			@ApiParam(value = "insertProductEntry", required = true) @Valid @RequestBody InsertProductRequestDTO insertProductRequestDTO) {
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

	}
}
