package com.minderest.service;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import com.minderest.model.SameProductsRequestDTO;
import com.minderest.model.SameProductsResponseDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.validation.Valid;

@Validated
@Api(value = "SameProducts", description = "the SameProducts API")
public interface SameProductsApi {

	default Optional<NativeWebRequest> getRequest() {
		return Optional.empty();
	}

	@ApiOperation(value = "Servicio que devuelve los productos equivalentes al introducido, en otros clientes", nickname = "sameProducts10", notes = "", response = SameProductsResponseDTO.class, tags = {
			"sameProducts" })

	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK.", response = SameProductsResponseDTO.class),
			@ApiResponse(code = 400, message = "Error validating entry params. "),
			@ApiResponse(code = 401, message = "Unauthorized"), @ApiResponse(code = 403, message = "Forbidden"),
			@ApiResponse(code = 404, message = "Not Found") })
	@RequestMapping(value = "/1.0/sameProducts", produces = { "application/json" }, consumes = {
			"application/json" }, method = RequestMethod.POST)
	default ResponseEntity<SameProductsResponseDTO> sameProducts10(
			@ApiParam(value = "sameProductsEntry", required = true) @Valid @RequestBody SameProductsRequestDTO sameProductsRequestDTO) {
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

	}
}
