package com.minderest.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.minderest.mapper.SameProductMapper;
import com.minderest.mapper.SameProductsResponseMapper;
import com.minderest.model.SameProduct;
import com.minderest.model.SameProductDTO;
import com.minderest.model.SameProductsResponse;
import com.minderest.model.SameProductsResponseDTO;

@Component
public class SameProductsResponseMapperImpl implements SameProductsResponseMapper {

	
	@Autowired
    private SameProductMapper sameProductMapper;
	
	@Override
	public SameProductsResponseDTO asSameProductsResponseDTO(SameProductsResponse src) {
		if (src == null) {
			return null;
		}

		SameProductsResponseDTO sameProductsResponseDTO = new SameProductsResponseDTO();
		
		sameProductsResponseDTO.setSameProducts(sameProductListToSameProductDTOList(src.getSameProducts()) );


		return sameProductsResponseDTO;
	}

	@Override
	public SameProductsResponse asSameProductsResponse(SameProductsResponseDTO src) {
		if (src == null) {
			return null;
		}

		SameProductsResponse sameProductsResponse = new SameProductsResponse();

		sameProductsResponse.setSameProducts( sameProductDTOListToSameProductList( src.getSameProducts() ) );

		return sameProductsResponse;
	}
	

    protected List<SameProductDTO> sameProductListToSameProductDTOList(List<SameProduct> list) {
        if ( list == null ) {
            return null;
        }

        List<SameProductDTO> list1 = new ArrayList<>( list.size() );
        for ( SameProduct sameProduct : list ) {
            list1.add( sameProductMapper.asSameProductDTO( sameProduct ) );
        }

        return list1;
    }

    protected List<SameProduct> sameProductDTOListToSameProductList(List<SameProductDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<SameProduct> list1 = new ArrayList<>( list.size() );
        for ( SameProductDTO sameProductDTO : list ) {
            list1.add( sameProductMapper.asSameProduct( sameProductDTO ) );
        }

        return list1;
    }
}
