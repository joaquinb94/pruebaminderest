package com.minderest.mapper;

import org.mapstruct.Mapper;

import com.minderest.model.SameProductsRequest;
import com.minderest.model.SameProductsRequestDTO;

@Mapper
public interface SameProductsRequestMapper {

	SameProductsRequestDTO asSameProductsRequestDTO(SameProductsRequest src);	
	SameProductsRequest asSameProductsRequest(SameProductsRequestDTO src);
	
}
