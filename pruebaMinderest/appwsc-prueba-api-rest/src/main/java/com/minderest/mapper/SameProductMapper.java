package com.minderest.mapper;

import org.mapstruct.Mapper;

import com.minderest.model.SameProduct;
import com.minderest.model.SameProductDTO;

@Mapper
public interface SameProductMapper {

	SameProductDTO asSameProductDTO(SameProduct src);	
	SameProduct asSameProduct(SameProductDTO src);
	
}
