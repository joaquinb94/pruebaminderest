package com.minderest.mapper.impl;

import org.springframework.stereotype.Component;

import com.minderest.mapper.SameProductMapper;
import com.minderest.model.SameProduct;
import com.minderest.model.SameProductDTO;

@Component
public class SameProductMapperImpl implements SameProductMapper {

	@Override
	public SameProductDTO asSameProductDTO(SameProduct src) {
		if (src == null) {
			return null;
		}

		SameProductDTO sameProductDTO = new SameProductDTO();

		sameProductDTO.setProductName(src.getProductName());
		sameProductDTO.setClientName(src.getClientName());

		return sameProductDTO;
	}

	@Override
	public SameProduct asSameProduct(SameProductDTO src) {
		if (src == null) {
			return null;
		}

		SameProduct sameProduct = new SameProduct();

		if (src.getProductName() != null) {
			sameProduct.setProductName(src.getProductName());
		}
		if (src.getClientName() != null) {
			sameProduct.setClientName(src.getClientName());
		}

		return sameProduct;
	}
}
