package com.minderest.mapper;

import org.mapstruct.Mapper;

import com.minderest.model.SameProductsResponse;
import com.minderest.model.SameProductsResponseDTO;

@Mapper(uses = {SameProductMapper.class})
public interface SameProductsResponseMapper {

	SameProductsResponseDTO asSameProductsResponseDTO(SameProductsResponse src);	
	SameProductsResponse asSameProductsResponse(SameProductsResponseDTO src);
	
}
