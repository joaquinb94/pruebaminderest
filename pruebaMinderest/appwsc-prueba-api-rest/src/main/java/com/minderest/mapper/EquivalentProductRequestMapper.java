package com.minderest.mapper;

import org.mapstruct.Mapper;

import com.minderest.model.EquivalentProductRequest;
import com.minderest.model.EquivalentProductRequestDTO;

@Mapper
public interface EquivalentProductRequestMapper {

	EquivalentProductRequestDTO asEquivalentProductRequestDTO(EquivalentProductRequest src);	
	EquivalentProductRequest asEquivalentProductRequest(EquivalentProductRequestDTO src);
	
}
