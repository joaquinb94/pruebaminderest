package com.minderest.mapper.impl;

import org.springframework.stereotype.Component;

import com.minderest.mapper.InsertProductRequestMapper;
import com.minderest.model.InsertProductRequest;
import com.minderest.model.InsertProductRequestDTO;

@Component
public class InsertProductRequestMapperImpl implements InsertProductRequestMapper {

	@Override
	public InsertProductRequestDTO asInsertProductRequestDTO(InsertProductRequest src) {
		if (src == null) {
			return null;
		}

		InsertProductRequestDTO insertProductRequestDTO = new InsertProductRequestDTO();

		insertProductRequestDTO.setProductId(src.getProductId());
		insertProductRequestDTO.setProductName(src.getProductName());
		insertProductRequestDTO.setClientId(src.getClientId());

		return insertProductRequestDTO;
	}

	@Override
	public InsertProductRequest asInsertProductRequest(InsertProductRequestDTO src) {
		if (src == null) {
			return null;
		}

		InsertProductRequest insertProductRequest = new InsertProductRequest();

		if (src.getProductId() != null) {
			insertProductRequest.setProductId(src.getProductId());
		}

		if (src.getProductName() != null) {
			insertProductRequest.setProductName(src.getProductName());
		}
		
		if (src.getClientId() != null) {
			insertProductRequest.setClientId(src.getClientId());
		}

		return insertProductRequest;
	}
}
