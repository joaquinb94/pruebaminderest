package com.minderest.mapper;

import org.mapstruct.Mapper;

import com.minderest.model.InsertProductRequest;
import com.minderest.model.InsertProductRequestDTO;

@Mapper
public interface InsertProductRequestMapper {

	InsertProductRequestDTO asInsertProductRequestDTO(InsertProductRequest src);	
	InsertProductRequest asInsertProductRequest(InsertProductRequestDTO src);
	
}
