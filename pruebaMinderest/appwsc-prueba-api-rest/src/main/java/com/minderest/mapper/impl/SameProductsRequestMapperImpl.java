package com.minderest.mapper.impl;

import org.springframework.stereotype.Component;

import com.minderest.mapper.SameProductsRequestMapper;
import com.minderest.model.SameProductsRequest;
import com.minderest.model.SameProductsRequestDTO;

@Component
public class SameProductsRequestMapperImpl implements SameProductsRequestMapper {

	@Override
	public SameProductsRequestDTO asSameProductsRequestDTO(SameProductsRequest src) {
		if (src == null) {
			return null;
		}

		SameProductsRequestDTO sameProductsRequestDTO = new SameProductsRequestDTO();

		sameProductsRequestDTO.setProductId(src.getProductId());
		sameProductsRequestDTO.setClientId(src.getClientId());

		return sameProductsRequestDTO;
	}

	@Override
	public SameProductsRequest asSameProductsRequest(SameProductsRequestDTO src) {
		if (src == null) {
			return null;
		}

		SameProductsRequest sameProductsRequest = new SameProductsRequest();

		if (src.getProductId() != null) {
			sameProductsRequest.setProductId(src.getProductId());
		}
		if (src.getClientId() != null) {
			sameProductsRequest.setClientId(src.getClientId());
		}

		return sameProductsRequest;
	}
}
