package com.minderest.mapper.impl;

import org.springframework.stereotype.Component;

import com.minderest.mapper.EquivalentProductRequestMapper;
import com.minderest.model.EquivalentProductRequest;
import com.minderest.model.EquivalentProductRequestDTO;

@Component
public class EquivalentProductRequestMapperImpl implements EquivalentProductRequestMapper {

	@Override
	public EquivalentProductRequestDTO asEquivalentProductRequestDTO(EquivalentProductRequest src) {
		if (src == null) {
			return null;
		}

		EquivalentProductRequestDTO equivalentProductRequestDTO = new EquivalentProductRequestDTO();

		equivalentProductRequestDTO.setProductId1(src.getProductId1());
		equivalentProductRequestDTO.setClientId(src.getClientId());
		equivalentProductRequestDTO.setProductId2(src.getProductId2());

		return equivalentProductRequestDTO;
	}

	@Override
	public EquivalentProductRequest asEquivalentProductRequest(EquivalentProductRequestDTO src) {
		if (src == null) {
			return null;
		}

		EquivalentProductRequest equivalentProductRequest = new EquivalentProductRequest();

		if (src.getProductId1() != null) {
			equivalentProductRequest.setProductId1(src.getProductId1());
		}
		if (src.getClientId() != null) {
			equivalentProductRequest.setClientId(src.getClientId());
		}
		if (src.getProductId2() != null) {
			equivalentProductRequest.setProductId2(src.getProductId2());
		}

		return equivalentProductRequest;
	}
}
