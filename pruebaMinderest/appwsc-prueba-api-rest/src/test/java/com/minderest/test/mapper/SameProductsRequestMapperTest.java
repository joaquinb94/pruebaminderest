package com.minderest.test.mapper;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.minderest.mapper.SameProductsRequestMapper;
import com.minderest.mapper.impl.SameProductsRequestMapperImpl;
import com.minderest.model.SameProductsRequest;
import com.minderest.model.SameProductsRequestDTO;

@ExtendWith(MockitoExtension.class)
class SameProductsRequestMapperTest {
	
	@InjectMocks
	private SameProductsRequestMapper sameProductsRequestMapper = new SameProductsRequestMapperImpl();

	
	
	@Test
	void sameProductsRequestMapperNullTest() throws Exception {
		SameProductsRequest result = sameProductsRequestMapper.asSameProductsRequest(null);
		assertNull(result);
	}

	
	@Test
	void sameProductsRequestMapperTest() throws Exception {

		SameProductsRequestDTO sameProductsRequestDTO = generarMockSameProductsRequestDTO();

		SameProductsRequest result = sameProductsRequestMapper.asSameProductsRequest(sameProductsRequestDTO);
		
		assertNotNull(result);
	}

	@Test
	void sameProductsRequestMapperToDTOTest() throws Exception {

		SameProductsRequest sameProductsRequest = generarMockSameProductsRequest();

		SameProductsRequestDTO result = sameProductsRequestMapper.asSameProductsRequestDTO(sameProductsRequest);
		
		assertNotNull(result);
	}
	
	
	@Test
	void sameProductsRequestMapperToDTONullTest() throws Exception {
		SameProductsRequestDTO result = sameProductsRequestMapper.asSameProductsRequestDTO(null);
		assertNull(result);
	}
	

	private SameProductsRequest generarMockSameProductsRequest() {

		SameProductsRequest result = new SameProductsRequest();

		result.setClientId(101);
		result.setProductId(1);
		

		return result;
	}
	
	private SameProductsRequestDTO generarMockSameProductsRequestDTO() {

		SameProductsRequestDTO result = new SameProductsRequestDTO();
		result.setClientId(101);
		result.setProductId(1);

		return result;
	}
	
}
