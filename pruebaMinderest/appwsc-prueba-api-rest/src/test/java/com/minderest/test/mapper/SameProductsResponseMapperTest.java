package com.minderest.test.mapper;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.minderest.mapper.SameProductMapper;
import com.minderest.mapper.SameProductsResponseMapper;
import com.minderest.mapper.impl.SameProductsResponseMapperImpl;
import com.minderest.model.SameProduct;
import com.minderest.model.SameProductDTO;
import com.minderest.model.SameProductsResponse;
import com.minderest.model.SameProductsResponseDTO;

@ExtendWith(MockitoExtension.class)
class SameProductsResponseMapperTest {

	@Mock
	private SameProductMapper sameProductMapper;
	
	@InjectMocks
	private SameProductsResponseMapper sameProductsResponseMapper = new SameProductsResponseMapperImpl();

	
	
	@Test
	void sameProductsResponseMapperNullTest() throws Exception {
		SameProductsResponse result = sameProductsResponseMapper.asSameProductsResponse(null);
		assertNull(result);
	}
	
	@Test
	void sameProductsResponseMapperListNullTest() throws Exception {

		SameProductsResponseDTO sameProductsResponseDTO = generarMockSameProductsResponseDTONull();

		SameProductsResponse result = sameProductsResponseMapper.asSameProductsResponse(sameProductsResponseDTO);
		
		assertNotNull(result);
	}

	
	@Test
	void sameProductsResponseMapperTest() throws Exception {

		SameProductsResponseDTO sameProductsResponseDTO = generarMockSameProductsResponseDTO();

		SameProductsResponse result = sameProductsResponseMapper.asSameProductsResponse(sameProductsResponseDTO);
		
		assertNotNull(result);
	}

	@Test
	void sameProductsResponseMapperToDTOTest() throws Exception {

		SameProductsResponse sameProductsResponse = generarMockSameProductsResponse();

		SameProductsResponseDTO result = sameProductsResponseMapper.asSameProductsResponseDTO(sameProductsResponse);
		
		assertNotNull(result);
	}
	
	
	@Test
	void sameProductsResponseMapperToDTOListNullTest() throws Exception {

		SameProductsResponse sameProductsResponse = generarMockSameProductsResponseNull();

		SameProductsResponseDTO result = sameProductsResponseMapper.asSameProductsResponseDTO(sameProductsResponse);
		
		assertNotNull(result);
	}
	
	
	@Test
	void sameProductsResponseMapperToDTONullTest() throws Exception {
		SameProductsResponseDTO result = sameProductsResponseMapper.asSameProductsResponseDTO(null);
		assertNull(result);
	}
	

	private SameProductsResponse generarMockSameProductsResponse() {

		SameProductsResponse result = new SameProductsResponse();

		List<SameProduct> list=new ArrayList<>();
		SameProduct sp = new SameProduct();
		sp.setClientName("Media Markt");
		sp.setProductName("Tostadora Inteligente");
		
		SameProduct sp2 = new SameProduct();
		sp2.setClientName("Worten");
		sp2.setProductName("Smart Tuestapan");
		
		list.add(sp);
		list.add(sp2);
		
		result.setSameProducts(list);
		

		return result;
	}
	
	
	private SameProductsResponse generarMockSameProductsResponseNull() {
		
		SameProductsResponse result = new SameProductsResponse();
		List<SameProduct> productsResult = null;
		result.setSameProducts(productsResult);
		return result;
	}
	
	private SameProductsResponseDTO generarMockSameProductsResponseDTO() {

		SameProductsResponseDTO result = new SameProductsResponseDTO();
		List<SameProductDTO> list=new ArrayList<>();
		SameProductDTO sp = new SameProductDTO();
		sp.setClientName("Media Markt");
		sp.setProductName("Tostadora Inteligente");
		
		SameProductDTO sp2 = new SameProductDTO();
		sp2.setClientName("Worten");
		sp2.setProductName("Smart Tuestapan");
		
		list.add(sp);
		list.add(sp2);
		
		result.setSameProducts(list);

		return result;
	}
	
	
	private SameProductsResponseDTO generarMockSameProductsResponseDTONull() {
		SameProductsResponseDTO result = new SameProductsResponseDTO();
		List<SameProductDTO> productsResult = null;
		result.setSameProducts(productsResult);
		return result;
	}
}
