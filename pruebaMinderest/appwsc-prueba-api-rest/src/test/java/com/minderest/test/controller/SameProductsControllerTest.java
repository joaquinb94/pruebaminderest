package com.minderest.test.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.minderest.controller.SameProductsController;
import com.minderest.mapper.SameProductsRequestMapper;
import com.minderest.mapper.SameProductsResponseMapper;
import com.minderest.model.SameProduct;
import com.minderest.model.SameProductDTO;
import com.minderest.model.SameProductsRequest;
import com.minderest.model.SameProductsRequestDTO;
import com.minderest.model.SameProductsResponse;
import com.minderest.model.SameProductsResponseDTO;
import com.minderest.service.SameProductsService;

@ExtendWith(MockitoExtension.class)
class SameProductsControllerTest {


	@Mock
	private SameProductsRequestMapper sameProductsRequestMapper;

	@Mock
	private SameProductsResponseMapper sameProductsResponseMapper;
	
	@InjectMocks
	private SameProductsController sameProductsController = new SameProductsController();

	@Mock
	private SameProductsService sameProductsService;

	@Test
	void sameProductsControllerTest() throws Exception {

		SameProductsResponse response = generarMockSameProductsResponse();

		Mockito.when(sameProductsService.sameProducts(Mockito.any(SameProductsRequest.class)))
				.thenReturn(response);
		
		Mockito.when(sameProductsRequestMapper
				.asSameProductsRequest(Mockito.any(SameProductsRequestDTO.class)))
				.thenReturn(generarMockSameProductsRequest());
		
		Mockito.when(sameProductsResponseMapper
				.asSameProductsResponseDTO(Mockito.any(SameProductsResponse.class)))
				.thenReturn(generarMockSameProductsResponseDTO());

		SameProductsRequestDTO sameProductsRequestDTO = generarMockSameProductsRequestDTO();

		ResponseEntity<SameProductsResponseDTO> sameProductsResponseDTO = sameProductsController
				.sameProducts10(sameProductsRequestDTO);
		assertEquals(HttpStatus.OK, sameProductsResponseDTO.getStatusCode());
	}

	
	private SameProductsRequest generarMockSameProductsRequest() {

		SameProductsRequest request = new SameProductsRequest();

		request.setClientId(101);
		request.setProductId(1);

		return request;

	}

	private SameProductsRequestDTO generarMockSameProductsRequestDTO() {

		SameProductsRequestDTO request = new SameProductsRequestDTO();

		request.setClientId(101);
		request.setProductId(1);

		return request;

	}
	
	
	private SameProductsResponse generarMockSameProductsResponse() {

		SameProductsResponse response = new SameProductsResponse();

		List<SameProduct> list=new ArrayList<SameProduct>();
		SameProduct sp = new SameProduct();
		sp.setClientName("Media Markt");
		sp.setProductName("Tostadora Inteligente");
		
		SameProduct sp2 = new SameProduct();
		sp2.setClientName("Worten");
		sp2.setProductName("Smart Tuestapan");
		
		list.add(sp);
		list.add(sp2);
		
		response.setSameProducts(list);

		return response;

	}
	
	private SameProductsResponseDTO generarMockSameProductsResponseDTO() {

		SameProductsResponseDTO response = new SameProductsResponseDTO();

		List<SameProductDTO> list=new ArrayList<SameProductDTO>();
		SameProductDTO sp = new SameProductDTO();
		sp.setClientName("Media Markt");
		sp.setProductName("Tostadora Inteligente");
		
		SameProductDTO sp2 = new SameProductDTO();
		sp2.setClientName("Worten");
		sp2.setProductName("Smart Tuestapan");
		
		list.add(sp);
		list.add(sp2);
		
		response.setSameProducts(list);

		return response;

	}
}
