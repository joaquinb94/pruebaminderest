package com.minderest.test.controller;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.minderest.controller.EquivalentProductController;
import com.minderest.mapper.EquivalentProductRequestMapper;
import com.minderest.model.EquivalentProductRequest;
import com.minderest.model.EquivalentProductRequestDTO;
import com.minderest.service.EquivalentProductService;

@ExtendWith(MockitoExtension.class)
class EquivalentProductControllerTest {


	@Mock
	private EquivalentProductRequestMapper equivalentProductRequestMapper;

	@InjectMocks
	private EquivalentProductController equivalentProductController = new EquivalentProductController();

	@Mock
	private EquivalentProductService equivalentProductService;

	@Test
	void equivalentProductControllerTest() throws Exception {

		// Mockito.doNothing().when(equivalentProductService.equivalentProduct(Mockito.any(EquivalentProductRequest.class)))

		Mockito.when(equivalentProductRequestMapper
				.asEquivalentProductRequest(Mockito.any(EquivalentProductRequestDTO.class)))
				.thenReturn(generarMockEquivalentProductRequest());

		EquivalentProductRequestDTO equivalentProductRequestDTO = generarMockEquivalentProductRequestDTO();

		ResponseEntity<Void> equivalentProductResponseDTO = equivalentProductController
				.equivalentProduct10(equivalentProductRequestDTO);
		assertEquals(HttpStatus.OK, equivalentProductResponseDTO.getStatusCode());
	}

	
	private EquivalentProductRequest generarMockEquivalentProductRequest() {

		EquivalentProductRequest request = new EquivalentProductRequest();

		request.setClientId(101);
		request.setProductId1(1);
		request.setProductId2(4);

		return request;

	}

	private EquivalentProductRequestDTO generarMockEquivalentProductRequestDTO() {

		EquivalentProductRequestDTO request = new EquivalentProductRequestDTO();

		request.setClientId(101);
		request.setProductId1(1);
		request.setProductId2(4);

		return request;

	}

}
