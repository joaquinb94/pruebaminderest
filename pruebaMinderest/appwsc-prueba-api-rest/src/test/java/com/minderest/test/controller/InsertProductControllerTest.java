package com.minderest.test.controller;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.minderest.controller.InsertProductController;
import com.minderest.mapper.InsertProductRequestMapper;
import com.minderest.model.InsertProductRequest;
import com.minderest.model.InsertProductRequestDTO;
import com.minderest.service.InsertProductService;

@ExtendWith(MockitoExtension.class)
class InsertProductControllerTest {


	@Mock
	private InsertProductRequestMapper insertProductRequestMapper;

	@InjectMocks
	private InsertProductController insertProductController = new InsertProductController();

	@Mock
	private InsertProductService insertProductService;

	@Test
	void insertProductControllerTest() throws Exception {

		Mockito.when(insertProductRequestMapper
				.asInsertProductRequest(Mockito.any(InsertProductRequestDTO.class)))
				.thenReturn(generarMockInsertProductRequest());

		InsertProductRequestDTO insertProductRequestDTO = generarMockInsertProductRequestDTO();

		ResponseEntity<Void> insertProductResponseDTO = insertProductController
				.insertProduct10(insertProductRequestDTO);
		assertEquals(HttpStatus.OK, insertProductResponseDTO.getStatusCode());
	}

	
	private InsertProductRequest generarMockInsertProductRequest() {

		InsertProductRequest request = new InsertProductRequest();

		request.setClientId(101);
		request.setProductId(1);
		request.setProductName("Tele 4K");

		return request;

	}

	private InsertProductRequestDTO generarMockInsertProductRequestDTO() {

		InsertProductRequestDTO request = new InsertProductRequestDTO();

		request.setClientId(101);
		request.setProductId(1);
		request.setProductName("Tele 4K");

		return request;

	}
}
