package com.minderest.test.mapper;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.minderest.mapper.InsertProductRequestMapper;
import com.minderest.mapper.impl.InsertProductRequestMapperImpl;
import com.minderest.model.InsertProductRequest;
import com.minderest.model.InsertProductRequestDTO;

@ExtendWith(MockitoExtension.class)
class InsertProductRequestMapperTest {

	@InjectMocks
	private InsertProductRequestMapper insertProductRequestMapper = new InsertProductRequestMapperImpl();

	
	
	@Test
	void insertProductRequestMapperNullTest() throws Exception {
		InsertProductRequest result = insertProductRequestMapper.asInsertProductRequest(null);
		assertNull(result);
	}

	
	@Test
	void insertProductRequestMapperTest() throws Exception {

		InsertProductRequestDTO insertProductRequestDTO = generarMockInsertProductRequestDTO();

		InsertProductRequest result = insertProductRequestMapper.asInsertProductRequest(insertProductRequestDTO);
		
		assertNotNull(result);
	}

	@Test
	void insertProductRequestMapperToDTOTest() throws Exception {

		InsertProductRequest insertProductRequest = generarMockInsertProductRequest();

		InsertProductRequestDTO result = insertProductRequestMapper.asInsertProductRequestDTO(insertProductRequest);
		
		assertNotNull(result);
	}
	
	
	@Test
	void insertProductRequestMapperToDTONullTest() throws Exception {
		InsertProductRequestDTO result = insertProductRequestMapper.asInsertProductRequestDTO(null);
		assertNull(result);
	}
	

	private InsertProductRequest generarMockInsertProductRequest() {

		InsertProductRequest result = new InsertProductRequest();

		result.setClientId(101);
		result.setProductId(1);
		result.setProductName("Tele 4K");
		

		return result;
	}
	
	private InsertProductRequestDTO generarMockInsertProductRequestDTO() {

		InsertProductRequestDTO result = new InsertProductRequestDTO();
		result.setClientId(101);
		result.setProductId(1);
		result.setProductName("Tele 4K");

		return result;
	}
}
