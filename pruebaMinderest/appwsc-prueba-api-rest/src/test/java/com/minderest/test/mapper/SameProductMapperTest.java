package com.minderest.test.mapper;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.minderest.mapper.SameProductMapper;
import com.minderest.mapper.impl.SameProductMapperImpl;
import com.minderest.model.SameProduct;
import com.minderest.model.SameProductDTO;

@ExtendWith(MockitoExtension.class)
class SameProductMapperTest {


	@InjectMocks
	private SameProductMapper sameProductMapper = new SameProductMapperImpl();

	
	
	@Test
	void sameProductMapperNullTest() throws Exception {
		SameProduct result = sameProductMapper.asSameProduct(null);
		assertNull(result);
	}

	
	@Test
	void sameProductMapperTest() throws Exception {

		SameProductDTO sameProductDTO = generarMockSameProductDTO();

		SameProduct result = sameProductMapper.asSameProduct(sameProductDTO);
		
		assertNotNull(result);
	}

	@Test
	void sameProductMapperToDTOTest() throws Exception {

		SameProduct sameProduct = generarMockSameProduct();

		SameProductDTO result = sameProductMapper.asSameProductDTO(sameProduct);
		
		assertNotNull(result);
	}
	
	
	@Test
	void sameProductMapperToDTONullTest() throws Exception {
		SameProductDTO result = sameProductMapper.asSameProductDTO(null);
		assertNull(result);
	}
	

	private SameProduct generarMockSameProduct() {

		SameProduct result = new SameProduct();

		result.setClientName("Media Markt");
		result.setProductName("Tostadora Inteligente");
		

		return result;
	}
	
	private SameProductDTO generarMockSameProductDTO() {

		SameProductDTO result = new SameProductDTO();
		result.setClientName("Media Markt");
		result.setProductName("Tostadora Inteligente");

		return result;
	}
	
}
