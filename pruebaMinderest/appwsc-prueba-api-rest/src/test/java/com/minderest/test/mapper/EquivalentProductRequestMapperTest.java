package com.minderest.test.mapper;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.minderest.mapper.EquivalentProductRequestMapper;
import com.minderest.mapper.impl.EquivalentProductRequestMapperImpl;
import com.minderest.model.EquivalentProductRequest;
import com.minderest.model.EquivalentProductRequestDTO;

@ExtendWith(MockitoExtension.class)
class EquivalentProductRequestMapperTest {

	@InjectMocks
	private EquivalentProductRequestMapper equivalentProductRequestMapper = new EquivalentProductRequestMapperImpl();

	
	
	@Test
	void equivalentProductRequestMapperNullTest() throws Exception {
		EquivalentProductRequest result = equivalentProductRequestMapper.asEquivalentProductRequest(null);
		assertNull(result);
	}

	
	@Test
	void equivalentProductRequestMapperTest() throws Exception {

		EquivalentProductRequestDTO equivalentProductRequestDTO = generarMockEquivalentProductRequestDTO();

		EquivalentProductRequest result = equivalentProductRequestMapper.asEquivalentProductRequest(equivalentProductRequestDTO);
		
		assertNotNull(result);
	}

	@Test
	void equivalentProductRequestMapperToDTOTest() throws Exception {

		EquivalentProductRequest equivalentProductRequest = generarMockEquivalentProductRequest();

		EquivalentProductRequestDTO result = equivalentProductRequestMapper.asEquivalentProductRequestDTO(equivalentProductRequest);
		
		assertNotNull(result);
	}
	
	
	@Test
	void equivalentProductRequestMapperToDTONullTest() throws Exception {
		EquivalentProductRequestDTO result = equivalentProductRequestMapper.asEquivalentProductRequestDTO(null);
		assertNull(result);
	}
	

	private EquivalentProductRequest generarMockEquivalentProductRequest() {

		EquivalentProductRequest result = new EquivalentProductRequest();

		result.setClientId(101);
		result.setProductId1(1);
		result.setProductId2(4);
		

		return result;
	}
	
	private EquivalentProductRequestDTO generarMockEquivalentProductRequestDTO() {

		EquivalentProductRequestDTO result = new EquivalentProductRequestDTO();
		result.setClientId(101);
		result.setProductId1(1);
		result.setProductId2(4);

		return result;
	}
}
