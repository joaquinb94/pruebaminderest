package com.minderest.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;


/**
 * The App configuration.
 */
@Configuration
@PropertySource({ "classpath:application.yml","classpath:sql.properties", "classpath:data.sql","classpath:schema-mysql.sql" })
public class AppConfiguration {
	@Bean(name = "mySQL")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.driverClassName("com.mysql.cj.jdbc.Driver");
        dataSourceBuilder.url("jdbc:mysql://127.0.0.1:3306/springbootdb");
        dataSourceBuilder.username("root");
        dataSourceBuilder.password("joaquinb94");
        return dataSourceBuilder.build();
    }

    @Bean
    public JdbcTemplate jdbcTemplate(@Qualifier("mySQL") final DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public NamedParameterJdbcTemplate namedJdbcTemplate(@Qualifier("mySQL") final DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

}
