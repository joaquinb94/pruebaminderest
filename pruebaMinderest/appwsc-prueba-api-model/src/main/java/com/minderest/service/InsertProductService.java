package com.minderest.service;

import com.minderest.model.InsertProductRequest;

public interface InsertProductService {

	public void insertProduct(InsertProductRequest insertProductRequest) throws Exception;

}
