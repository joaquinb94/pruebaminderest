package com.minderest.service;

import com.minderest.model.SameProductsRequest;
import com.minderest.model.SameProductsResponse;

public interface SameProductsService {

	public SameProductsResponse sameProducts(SameProductsRequest sameProductsRequest) throws Exception;

}
