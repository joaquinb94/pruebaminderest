package com.minderest.service;

import com.minderest.model.EquivalentProductRequest;

public interface EquivalentProductService {

	public void equivalentProduct(EquivalentProductRequest equivalentProductRequest) throws Exception;

}
