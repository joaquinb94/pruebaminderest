package com.minderest.model;

public class EquivalentProductRequest {

	private Integer productId1;
	private Integer clientId;
	private Integer productId2;
	
	
	public EquivalentProductRequest() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getProductId1() {
		return productId1;
	}


	public void setProductId1(Integer productId1) {
		this.productId1 = productId1;
	}


	public Integer getClientId() {
		return clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Integer getProductId2() {
		return productId2;
	}


	public void setProductId2(Integer productId2) {
		this.productId2 = productId2;
	}
	
	
	
}
