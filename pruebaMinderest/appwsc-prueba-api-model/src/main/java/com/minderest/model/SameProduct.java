package com.minderest.model;

public class SameProduct {

	private String clientName;
	private String productName;
	
	public SameProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	
}
