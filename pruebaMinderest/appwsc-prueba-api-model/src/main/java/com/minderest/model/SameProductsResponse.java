package com.minderest.model;

import java.util.List;

public class SameProductsResponse {

	private List<SameProduct> sameProducts;

	public SameProductsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<SameProduct> getSameProducts() {
		return sameProducts;
	}

	public void setSameProducts(List<SameProduct> sameProducts) {
		this.sameProducts = sameProducts;
	}
	
}
