package com.minderest.exception;

public class ProductException extends Exception{

	private static final long serialVersionUID = -7594238418357595166L;

    public ProductException(final String arg0) {
        super(arg0);
    }

    public ProductException(final Throwable cause) {
        super(cause);
    }

    public ProductException(final String arg0, final Throwable cause) {
        super(arg0, cause);
    }
	
}
